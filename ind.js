import React from 'react';
import { render } from 'react-dom';
import { Stage, Layer, Rect, Arrow, Line} from 'react-konva';

import "./index.css"


const box1_default = {
  x: 200,
  y: 100,
  width: 150,
  height: 150,
  fill: '#414242',
  cornerRadius: 10,
  draggable: true,
  dragBoundFunc: function (pos) {
    var newX = pos.x < 50 ? 50 : pos.x;
    var newY = pos.y < 50 ? 50 : pos.y;
    return {
      x: newX,
      y: newY,
    };
  },
}
const box2_default = {
  x: 600,
  y: 100,
  width: 150,
  height: 150,
  fill: '#414242',
  cornerRadius: 10,
  draggable: true,
  dragBoundFunc: function (pos) {
    var newY = pos.y < 50 ? 50 : pos.y;
    var newX = pos.x < 500 ? 500: pos.x;
    return {
      x: newX,
      y: newY,
    }
  }
}

const box3_default = {
  x: 600,
  y: 300,
  width: 150,
  height: 150,
  fill: '#414242',
  cornerRadius: 10,
  draggable: true,
  dragBoundFunc: function (pos) {
    var newY = pos.y < 270 ? 270 : pos.y;
    return {
      x: pos.x,
      y: newY,
    }
  }
}
const box4_default = {
  x: 1000,
  y: 500,
  width: 150,
  height: 150,
  fill: '#414242',
  cornerRadius: 10,
  draggable: true,
  dragBoundFunc: function (pos) {
    var newX = pos.x < 950 ? 950 : pos.x;
    return {
      x: newX,
      y: pos.y,
    }
  }
}




const Edge = ({node1, node2}) => {

  const RADIUS = 20;

  return (
    <Shape
    sceneFunc={(context, shape) => {
      const width = node2.x - node1.x;
      const height = node2.y - node1.y;
      const dir = Math.sign(height);
      const radius = Math.min(
        RADIUS,
        Math.abs(height / 2),
        Math.abs(width / 2)
      );

      context.beginPath();
      context.moveTo(node1.x+150, node1.y+50);
      context.lineTo(node1.x+150 + width / 2 - RADIUS, node1.y+50);
      context.quadraticCurveTo(
        node1.x+150 + width / 2,
        node1.y+50,
        node1.x+150 + width / 2,
        node1.y+50 + dir * radius
      );
      
      context.lineTo(node1.x+150 + width / 2, node2.y+50 - dir * radius);
      context.quadraticCurveTo(
        node1.x+150 + width / 2,
        node2.y+50, 
        node1.x+150 + width / 2 + radius,
        node2.y+50
      );
      context.lineTo(node2.x, node2.y+50);
      context.fillStrokeShape(shape);
    }}
    stroke="black"
    strokeWidth={2}
  />)
};



const App = () => {
  const [box1, updateBox1] = React.useState(box1_default);
  const [box2, updateBox2] = React.useState(box2_default);
  const [box3, updateBox3] = React.useState(box3_default);
  const [box4, updateBox4] = React.useState(box4_default);

  return (
    <div className="index">
      <Stage className="index-rect" width={window.innerWidth} height={window.innerHeight}>
        <Layer >
          <Edge node1={box1} node2={box2}/>
          <Edge node1={box1} node2={box3}/>
          <Edge node1={box3} node2={box4}/>
          <Rect 
            {...box1}
            onDragMove={e => {
              updateBox1({ ...box1, ...e.target.position() });
            }}
          />
          <Rect 
            {...box2}
            onDragMove={e => {
              updateBox2({ ...box2, ...e.target.position() });
            }}
          />
          <Rect 
            {...box3}
            onDragMove={e => {
              updateBox3({ ...box3, ...e.target.position() });
            }}
          />
          <Rect 
            {...box4}
            onDragMove={e => {
              updateBox4({ ...box4, ...e.target.position() });
            }}
          />
        </Layer>
      </Stage>
      </div>
  );
};

render(<App />, document.getElementById('root'));
